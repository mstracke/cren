// ren.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"


LPCWSTR lpNewProc;

void printpause(wchar_t *msg, int errorCode);

const size_t BUFFER_SIZE = 256;

void _tmain(int argc, TCHAR *argv[])
{
	STARTUPINFO si;
	PROCESS_INFORMATION pi;

	if (argc == 4) 
	{
		//printf("Renaming: %ls to %ls\n", argv[1], argv[2]);
		if (_wrename(argv[1], argv[2]) == 0)
		{
			ZeroMemory(&si, sizeof(si));
			si.cb = sizeof(si);
			ZeroMemory(&pi, sizeof(pi));

			
			//printf("Starting %ls\n", argv[2]);
			
			TCHAR fargs[BUFFER_SIZE];
			wcscpy_s(fargs, argv[2]);
			wcscat_s(fargs, L" ");
			wcscat_s(fargs, argv[3]);

			if (!CreateProcess(NULL,
				fargs,
				NULL, NULL, FALSE,
				0, NULL, NULL,
				&si,
				&pi)
			)
			{
				printpause(_T("CreateProcess failed."), GetLastError());
				return;
			}
		}
		else
		{
			printpause(_T("Rename file failed."), GetLastError());
			return;
		}
	}
	else
	{
		printf("Usage: %ls [oldpath] [newpath]\n", argv[0]);
		std::cin.get();
		return;
	}
	return;
}

void printpause(wchar_t *msg, int errorCode)
{
	printf("%ls (%d)\n", msg, errorCode);
	std::cin.get();
}

